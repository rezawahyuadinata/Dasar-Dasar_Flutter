import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class DasarWidget extends StatelessWidget {
  const DasarWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          drawer: buildDrawer(context),
          appBar: AppBar(
            title: Text("penggunaan widget"),
          ),
          body: Center(
            child: Container(
                color: Colors.lightBlue,
                width: 150,
                child: Text(
                  "hello world, nama saya reza wahyu adinata",
                  style: TextStyle(
                      color: Colors.amber,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  softWrap: false,
                  textAlign: TextAlign.center,
                )),
          )),
    );
  }
}
