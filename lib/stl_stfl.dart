import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class StlStfl extends StatefulWidget {
  const StlStfl({Key? key}) : super(key: key);

  @override
  _StlStflState createState() => _StlStflState();
}

class _StlStflState extends State<StlStfl> {
  int number = 0;
  void tekantombol() {
    setState(() {
      number = number + 1;
    });
  }

  void tekantombol1() {
    setState(() {
      number = number - 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text(
            "pengenalan penggunaan state less dan full",
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                number.toString(),
                style: TextStyle(fontSize: 10 + number.toDouble()),
              ),
              RaisedButton(
                  child: Text("tekan tombol +"), onPressed: tekantombol),
              RaisedButton(
                  child: Text("tekan tombol -"), onPressed: tekantombol1)
            ],
          ),
        ),
      ),
    );
  }
}
