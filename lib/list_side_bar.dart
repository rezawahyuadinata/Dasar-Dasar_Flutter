import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListSideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          //DrawerHeader
          const DrawerHeader(
            child: Text('Tutorial Pembelajaran Flutter'),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          //Home
          ListTile(
            dense: true,
            title: Text('Home'),
            leading: Icon(Icons.home),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Animasi Container'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/animasicontainer');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Anonymous Method'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/anonymousmethod');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Container Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/containermethod');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Dasar Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/dasarwidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Flexible Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/flexiblewidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Image Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/imagewidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('List dan ListView'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/listview');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Pengenalan'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/pengenalan');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Row dan Column'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/rowdancolumn');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Stack dan Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/stackalignwidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Stateless/Stateful Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/stlstfl');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Text Style'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/textstyle');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Spacer Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/spacerwidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('DragAble/target,SizedBox,and Material'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/dragable');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Navigation Page'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/navigation');
            },
          ),
          ListTile(
            dense: true,
            title: Text('AppBar Gradasi'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/appbargradasi');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Card Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/cardwidget');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Text Field Widget'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/textfield');
            },
          ),
          ListTile(
            dense: true,
            title: Text('MediaQuery'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/mediaquery');
            },
          ),
          ListTile(
            dense: true,
            title: Text('InkWell'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/inkwell');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Opacity'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/opacity');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Positioned, Floating Button, Login Page'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/positioned');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Hero Clip Rect'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/herocliprect');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Custom Height Appbar'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/customheight');
            },
          ),
          ListTile(
            dense: true,
            title: Text('QR Code'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/qrcode');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Custom Button'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/custombutton');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Gradient Opacity'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/gradientopacity');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Play Music'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/playmusic');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Scan QR'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/scanqr');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Font Feature'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/fontfeature');
            },
          ),
          ListTile(
            dense: true,
            title: Text('Clip Path'),
            leading: Icon(Icons.label),
            onTap: () {
              Navigator.pushNamed(context, '/clippath');
            },
          ),
        ],
      ),
    );
  }
}
