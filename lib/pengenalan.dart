import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class Pengenalan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text("Aplikasi Flutter Pertama Saya"),
        ),
        body: Text("Hello World"),
      ),
    );
  }
}
