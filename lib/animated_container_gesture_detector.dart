import 'package:flutter/material.dart';
import 'dart:math';
import 'routes.dart';

class AnimasiContainer extends StatefulWidget {
  const AnimasiContainer({Key? key}) : super(key: key);

  @override
  _AnimasiContainerState createState() => _AnimasiContainerState();
}

class _AnimasiContainerState extends State<AnimasiContainer> {
  Random random = Random();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text(
            "pembelajaran AnimatedContainer dan GestureDetector",
            maxLines: 2,
          ),
        ),
        body: Center(
          child: GestureDetector(
            onTap: () {
              setState(() {});
            },
            child: AnimatedContainer(
              color: Color.fromARGB(
                255,
                random.nextInt(256),
                random.nextInt(256),
                random.nextInt(256),
              ),
              duration: Duration(seconds: 1),
              width: 50.0 + random.nextInt(101),
              height: 50.0 + random.nextInt(101),
            ),
          ),
        ),
      ),
    );
  }
}
