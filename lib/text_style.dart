import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class TextSTYLE extends StatelessWidget {
  const TextSTYLE({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text("latihan Text Style"),
        ),
        body: Center(
          child: Text(
            "Ini Adalah Text",
            style: TextStyle(
                color: Colors.amber,
                fontStyle: FontStyle.italic,
                fontSize: 29,
                decoration: TextDecoration.overline,
                decorationThickness: 5,
                decorationStyle: TextDecorationStyle.dotted),
          ),
        ),
      ),
    );
  }
}
