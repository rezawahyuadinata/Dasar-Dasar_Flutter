import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class AppbarGradasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text(
            "Appbar Gradasi",
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.settings),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.exit_to_app),
            ),
          ],
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.yellow, Colors.amber],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
              //memeberikan efek gradasi/Background pada appbar menggunakan gambar
              image: DecorationImage(
                  image: AssetImage("assets/pattern.png"),
                  fit: BoxFit.none,
                  repeat: ImageRepeat.repeat),
            ),
          ),
        ),
      ),
    );
  }
}
