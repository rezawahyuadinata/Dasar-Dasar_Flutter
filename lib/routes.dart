import 'package:aplikasi_pembelajaran/flexible_widget.dart';
import 'package:aplikasi_pembelajaran/inkwell.dart';
import 'package:aplikasi_pembelajaran/play_music.dart';
import 'package:aplikasi_pembelajaran/positioned.dart';
import 'package:aplikasi_pembelajaran/qrcode.dart';
import 'package:aplikasi_pembelajaran/scanqr.dart';
import 'package:aplikasi_pembelajaran/tabbar.dart';
import 'package:aplikasi_pembelajaran/textfieldwidget.dart';
import 'package:aplikasi_pembelajaran/ubah_tabbar.dart';
import 'clippath.dart';
import 'custom_button.dart';
import 'custom_height.dart';
import 'font_feature.dart';
import 'gradient_opacity.dart';
import 'herocliprect.dart';
import 'list_side_bar.dart';
import 'package:flutter/material.dart';
import 'appbar_gradasi.dart';
import 'card_widget.dart';
import 'home.dart';
import 'animated_container_gesture_detector.dart';
import 'anonymous_method.dart';
import 'container_widget.dart';
import 'dasar_widget.dart';
import 'image_widget.dart';
import 'list_dan_listview.dart';
import 'mediaquery.dart';
import 'opacity.dart';
import 'pengenalan.dart';
import 'row_column.dart';
import 'stack_widget_dan_align_widget.dart';
import 'stl_stfl.dart';
import 'text_style.dart';
import 'spacer_widget.dart';
import 'dragable_sizedbox_material.dart';
import 'navigation.dart';

class Routes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Tutorial Pembelajaran",
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) {
          return Home();
        },
        '/animasicontainer': (BuildContext context) {
          return AnimasiContainer();
        },
        '/anonymousmethod': (BuildContext context) {
          return AnonymousMethod();
        },
        '/containerwidget': (BuildContext context) {
          return ContainerWidget();
        },
        '/dasarwidget': (BuildContext context) {
          return DasarWidget();
        },
        '/flexiblewidget': (BuildContext context) {
          return FlexibleWidget();
        },
        '/imagewidget': (BuildContext context) {
          return ImageWidget();
        },
        '/listdanlistview': (BuildContext context) {
          return ListListView();
        },
        '/pengenalan': (BuildContext context) {
          return Pengenalan();
        },
        '/rowcolumn': (BuildContext context) {
          return RowColumn();
        },
        '/stackalignwidget': (BuildContext context) {
          return StackAlignWidget();
        },
        '/stlstfl': (BuildContext context) {
          return StlStfl();
        },
        '/textstyle': (BuildContext context) {
          return TextSTYLE();
        },
        '/spacerwidget': (BuildContext context) {
          return SpacerWidget();
        },
        '/dragable': (BuildContext context) {
          return DragAble();
        },
        '/navigation': (BuildContext context) {
          return PindahHalaman();
        },
        '/appbargradasi': (BuildContext context) {
          return AppbarGradasi();
        },
        '/cardwidget': (BuildContext context) {
          return CardWidget();
        },
        '/textfield': (BuildContext context) {
          return TextfieldWidget();
        },
        '/mediaquery': (BuildContext context) {
          return Mediaquery();
        },
        '/inkwell': (BuildContext context) {
          return InkWells();
        },
        '/opacity': (BuildContext context) {
          return OPacity();
        },
        '/positioned': (BuildContext context) {
          return POsitioned();
        },
        '/herocliprect': (BuildContext context) {
          return HeroClipRect();
        },
        '/customheight': (BuildContext context) {
          return CustomHeight();
        },
        '/tabbar': (BuildContext context) {
          return Tabbar();
        },
        '/ubahtabbar': (BuildContext context) {
          return Ubahtabbar();
        },
        '/qrcode': (BuildContext context) {
          return Qrcode();
        },
        '/custombutton': (BuildContext context) {
          return Custombutton();
        },
        '/gradientopacity': (BuildContext context) {
          return GradientOpacity();
        },
        '/playmusic': (BuildContext context) {
          return PlayMusic();
        },
        '/scanqr': (BuildContext context) {
          return Scanqr();
        },
        '/fontfeature': (BuildContext context) {
          return Fontfeature();
        },
        '/clippath': (BuildContext context) {
          return Clippath();
        },
      },
    );
  }
}

Widget buildDrawer(BuildContext context) {
  return ListSideBar();
}
