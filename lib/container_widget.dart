import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class ContainerWidget extends StatelessWidget {
  const ContainerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          drawer: buildDrawer(context),
          appBar: AppBar(
            title: Text("pengenalan container widget"),
          ),
          body: Container(
            color: Colors.red,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.blue,
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[Colors.amber, Colors.blue])),
              margin: EdgeInsets.fromLTRB(23, 10, 23, 10),
              padding: EdgeInsets.only(right: 22),
            ),
          )),
    );
  }
}
