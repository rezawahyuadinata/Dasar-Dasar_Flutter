import 'package:flutter/material.dart';

class Mediaquery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Media(),
    );
  }
}

class Media extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Media Query"),
      ),
      body: (MediaQuery.of(context).orientation == Orientation.portrait)
          ? Column(
              children: generateContainer(context),
            )
          : Row(
              children: generateContainer(context),
            ),
    );
  }

  List<Widget> generateContainer(BuildContext context) {
    return <Widget>[
      Container(color: Colors.red, width: 100, height: 100),
      Container(color: Colors.yellow, width: 100, height: 100),
      Container(color: Colors.blue, width: 100, height: 100),
    ];
  }
}
