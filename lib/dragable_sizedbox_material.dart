import 'package:aplikasi_pembelajaran/routes.dart';
import 'package:flutter/material.dart';

class DragAble extends StatefulWidget {
  const DragAble({Key? key}) : super(key: key);

  @override
  _DragAbleState createState() => _DragAbleState();
}

class _DragAbleState extends State<DragAble> {
  Color color1 = Colors.red;
  Color color2 = Colors.blue;
  late Color targetColor;
  bool isAccept = false;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: buildDrawer(context),
        appBar: AppBar(
          title: Text("Dragable,Sizedbox, and Material"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Draggable<Color>(
                  data: color1,
                  child: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: color1, shape: StadiumBorder(), elevation: 0),
                  ),
                  childWhenDragging: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: Colors.black,
                        shape: StadiumBorder(),
                        elevation: 0),
                  ),
                  feedback: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: Colors.green.withOpacity(0.5),
                        shape: StadiumBorder(),
                        elevation: 0),
                  ),
                ),
                Draggable<Color>(
                  data: color2,
                  child: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: color2, shape: StadiumBorder(), elevation: 0),
                  ),
                  childWhenDragging: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: Colors.black,
                        shape: StadiumBorder(),
                        elevation: 0),
                  ),
                  feedback: SizedBox(
                    width: 50,
                    height: 50,
                    child: Material(
                        color: Colors.green.withOpacity(0.5),
                        shape: StadiumBorder(),
                        elevation: 0),
                  ),
                ),
              ],
            ),
            DragTarget<Color>(
              onWillAccept: (value) => true,
              onAccept: (value) {
                isAccept = true;
                targetColor = value;
              },
              builder: (context, candidates, rejected) {
                return (isAccept)
                    ? SizedBox(
                        width: 100,
                        height: 100,
                        child: Material(
                            color: targetColor,
                            shape: StadiumBorder(),
                            elevation: 0),
                      )
                    : SizedBox(
                        width: 50,
                        height: 50,
                        child: Material(
                            color: Colors.black,
                            shape: StadiumBorder(),
                            elevation: 0),
                      );
              },
            ),
          ],
        ),
      ),
    );
  }
}
